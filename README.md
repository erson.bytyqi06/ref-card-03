# Ref-Card-03

Die vorliegende Anwendung ist eine Java-basierte Architektur-Referenzkarte für SRE (Site Reliability Engineering). Sie ermöglicht das Abrufen und Verwalten von Witzen in verschiedenen Kategorien über eine RESTful-Schnittstelle und verwendet eine Datenbank zur Speicherung der Witze.

## Voraussetzungen

Sie brauchen eine UNIX-basierte Maschine (Linux, MacOS oder WSL), Docker und Git, um das Projekt auszuführen.

## Projekt installieren

Projekt herunterladen

```sh
git clone https://gitlab.com/bbwrl/m346-ref-card-03.git
cd m346-ref-card-03
```

Docker-Image erstellen

```sh
$ docker build -t refcard-03 .
```

## Projekt starten

Um das Projekt zu starten, müssen Sie folgende Schritte befolgen.

```sh
$ docker run refcard-03
```

Im Browser ist die App unter der URL http://localhost:8080 erreichbar.

# Pipeline

Die Pipeline besteht aus zwei Phasen: "package" und "deploy". In der "package"-Phase wird das erstellte Docker-Image in das ECR (Elastic Container Registry)-Repository übertragen. In der "deploy"-Phase wird die aktualisierte Version des Images für den Service bereitgestellt.

## Voraussetzungen:

- Du benötigst eine UNIX-basierte Maschine (Linux, MacOS oder WSL).
- Stelle sicher, dass die neueste Version von Docker und Git installiert ist.
- Du musst die AWS CLI (Command Line Interface) verwenden, um eine Verbindung zu AWS herzustellen.
- Die Pipeline-Konfiguration enthält sensible Informationen, die als Umgebungsvariablen in den Einstellungen der Pipeline angegeben werden sollen.

## Umgebungsvariablen:

- AWS_ACCESS_KEY_ID: AWS Access Key ID für den Zugriff auf AWS-Ressourcen.
- AWS_SECRET_ACCESS_KEY: Geheimer Zugriffsschlüssel, der mit der Access Key ID verknüpft ist.
- AWS_SESSION_TOKEN: Authentifizierung für temporäre Sitzungen mit erweiterten AWS-Berechtigungen.
- AWS_DEFAULT_REGION: Die AWS-Region, die verwendet wird.
- CI_AWS_ECR_REGISTRY: URL zur ECS (Elastic Container Service) Registry.
- CI_AWS_ECR_REPOSITORY_NAME: Der Name des Repositorys in der ECR.

## AWS-Setup

- Erstelle eine RDS-Datenbank und gib den ursprünglichen Datenbanknamen auf der Registerkarte "additional configuration" an.
- Erstelle eine Task-Definition für ECS und füge die erforderlichen Umgebungsvariablen hinzu, wie z.B. DB_URL, DB_USERNAME und DB_PASSWORD.

## Dockerfile:

- In der Dockerdatei werden die Umgebungsvariablen mit den Werten aus der Task-Definition verknüpft, um die Verbindung zur Datenbank herzustellen.
- Die Werte der Umgebungsvariablen werden als Systemeigenschaften an die Java-Anwendung übergeben.
